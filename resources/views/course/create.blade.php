@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <br>
            <div class="card">
                <div class="card-title text-center">
                    <br>
                    <span class="pt-4 text-primary">Crear Curso</span>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('course.create') }}">
                        @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">Titulo</label>
                            <input type="text" name="title" class="form-control" placeholder="Titulo">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Descripcion</label>
                            <textarea name="description" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Cupos</label>
                            <input type="Number" name="places" class="form-control" placeholder="places">
                        </div>
                        <br>
                        <div class="row">
                            <label for="formGroupExampleInput">Desde</label>
                            <div class="col">
                            <input type="date" name="startdate" class="form-control" placeholder="Fecha">
                            </div>
                            <div class="col">
                            <input type="time" name="starttime" class="form-control" placeholder="Hora">
                            </div>
                        </div>
                        <div class="row">
                            <label for="formGroupExampleInput">Hasta</label>
                            <div class="col">
                            <input type="date" name="enddate" class="form-control" placeholder="Fecha">
                            </div>
                            <div class="col">
                            <input type="time" name="endtime" class="form-control" placeholder="Hora">
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-success">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Descripcion Curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
        </div>

    </div>
</div>
</div>


@endsection

@section('js')

@endsection