<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('course.index');
    }
    public function list()
    {
        $courses = Course::all();

        return datatables($courses)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['startdate'] = $request->startdate.':'.$request->starttime;
        $request['enddate'] = $request->enddate.':'.$request->endtime;
        Course::create($request->except('starttime','endtime'));
        return redirect()->route('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::where('id', $id)->first();

        // return datatables($books)->toJson();
        return response()->json($course);
    }

    public function inscription(Course $course)
    {   
        // $request->validate(['days' => 'required']);
        // return response()->json('Hola');
        $course->decrement('places');
        auth()->user()->courses()->attach($course);
        $course->save();
        return response()->json(['res' => 'Inscrito con Exito'],200);
        // return redirect()->route('home')->with('swal', ['title' => 'Exito!', 'message' => strtoupper('Libro Reservado'), 'status' => 'success', 'showDenyButton' => 'true']);;
    }

    public function uninscription(Course $course)
    {   
        // $request->validate(['days' => 'required']);
        // return response()->json('Hola');
        $course->increment('places');
        auth()->user()->courses()->detach($course);
        $course->save();
        return response()->json(['res' => 'Suscripcion Eliminada'],200);
        // return redirect()->route('home')->with('swal', ['title' => 'Exito!', 'message' => strtoupper('Libro Reservado'), 'status' => 'success', 'showDenyButton' => 'true']);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
