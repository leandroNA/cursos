<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\CourseController::class, 'index'])->name('home')->can('course.show');
Route::get('course/list', [App\Http\Controllers\CourseController::class, 'list'])->name('course.list')->can('course.show');
Route::get('/course/{course}', [App\Http\Controllers\CourseController::class, 'show'])->name('course.show')->can('course.show');;
Route::post('/course/{course}', [App\Http\Controllers\CourseController::class, 'inscription'])->name('book.incription')->can('course.show');
Route::post('/course/desuscribir/{course}', [App\Http\Controllers\CourseController::class, 'uninscription'])->name('book.unincription')->can('course.show');
Route::get('/course', [App\Http\Controllers\CourseController::class, 'create'])->name('course.create')->can('course.create');;
Route::post('/course', [App\Http\Controllers\CourseController::class, 'store'])->name('course.store')->can('course.store');;

