<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::now();
        Course::create([
            'title' => 'PHP',
            'description' => 'Curso de PHP',
            'places' => 10,
            'startdate' => $date,
            'enddate' => $date->addDay(1),
        ]);
        Course::create([
            'title' => 'Laravel',
            'description' => 'Curso de PHP Laravel',
            'places' => 10,
            'startdate' => $date,
            'enddate' => $date->addDay(1),
        ]);
    }
}
