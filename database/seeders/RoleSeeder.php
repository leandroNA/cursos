<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $student = Role::create(['name' => 'student']);

        Permission::create(['name' => 'course.index'])->syncRoles([$admin,$student]);
        Permission::create(['name' => 'course.show'])->syncRoles([$admin,$student]);
        Permission::create(['name' => 'course.create'])->syncRoles([$admin]);
        Permission::create(['name' => 'course.store'])->syncRoles([$admin]);
        Permission::create(['name' => 'course.edit'])->syncRoles([$admin]);
        Permission::create(['name' => 'course.destroy'])->syncRoles([$admin]);  

    }
}
